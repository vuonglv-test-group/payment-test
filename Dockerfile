FROM python:3.6-slim-buster

COPY requirements.txt /tmp/requirements.txt

RUN pip install -r /tmp/requirements.txt

WORKDIR /app
COPY . .

EXPOSE 8000

CMD [ "gunicorn", "vnpay_python.wsgi", "-b", "0.0.0.0:8000" ]
